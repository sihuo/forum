# IT私活论坛

**发布需求、资源对接、接单、接项目**

*程序员互相对接，资源共赢*

# 流程
## 简单粗暴

1、发布需求

2、接单

## 复杂稳定
1、发布需求

2、团队磨合

3、接项目

# 论坛地址

## 论坛
[论坛地址](https://gitlab.com/sihuo/forum/issues)

## 微信群

![wechat group](https://img.ubispot.com/images/2019/04/09/Image-from-iOS.jpg)

过期的话，加vx dealer3000进群